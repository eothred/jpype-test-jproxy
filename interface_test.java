
class MyTest {
    public static void testFun(MyInterface someInstance) {
        System.out.format("5*5 is %f%n",someInstance.Squared(5));
    }

    public static void testFun2(MyInterface someInstance) {
        aO myAO = new aO();
        someInstance.printSome(myAO);
    }
}

class MyImplementation implements MyInterface {
    public double Squared (double number) {
        return number*number;
    }
    public void printSome(AnotherInterface aO) {
        aO.helloWorld();
    }
}

class aO implements AnotherInterface {
    public void helloWorld() {
        System.out.println("World, Hello");
    }
}

class MyMain {
    public static void main(String[] args) {
        MyImplementation myImpl = new MyImplementation();
        aO myAO = new aO();
        MyTest.testFun(myImpl);
        myImpl.printSome(myAO);
    }
}
