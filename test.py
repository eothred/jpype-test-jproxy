import jpype
import os

# OSX:
JVM_LIB  = os.path.join( os.getenv('JAVA_HOME'),
                                'jre/lib/server/libjvm.dylib')
# More general:
# JVM_LIB = jpype.getDefaultJVMPath()

pwd=os.path.realpath('.')

jpype.startJVM(JVM_LIB,
               "-ea",
               "-Djava.class.path="+pwd)

class mImplementation():
    def Squared(self, number):
        '''
        no idea how to calculate square..
        '''
        return 4+number**2
    def printSome(self, anotherInterface):
        print("Hello, World")
        anotherInterface.helloWorld()

my_impl = mImplementation()

my_proxy = jpype.JProxy("MyInterface", inst = my_impl)

jpype.JClass("MyTest").testFun(my_proxy)
jpype.JClass("MyTest").testFun2(my_proxy)

jpype.shutdownJVM()
